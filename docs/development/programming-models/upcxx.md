# UPC++

[UPC++](https://upcxx.lbl.gov) is a C++ library that supports
Partitioned Global Address Space (PGAS) programming, and is designed
to interoperate smoothly and efficiently with MPI, OpenMP, CUDA and
AMTs. It leverages GASNet-EX to deliver low-overhead, fine-grained
communication, including Remote Memory Access (RMA) and Remote
Procedure Call (RPC).

## Using UPC++ at NERSC

On Cori, UPC++ is available by loading the `upcxx` module.

On Perlmutter, you first must load the `contrib` module, 
which will then make the `upcxx` module available to load:

```shell
perlmutter$ module load contrib upcxx
```

One can see the available UPC++ versions using the following command:

```shell
nersc$ module avail upcxx
```

The default version is the latest stable release of UPC++, which
is usually the best choice for most purposes. There are also
variants named `upcxx-cuda` that include UPC++ library support 
for communication involving CUDA device memory, accelerated using 
GPUDirect RDMA hardware support.

Also see 
[UPC++ documentation for Perlmutter](https://bitbucket.org/berkeleylab/upcxx/wiki/docs/system/perlmutter)
for site-specific details and recommendations regarding use of UPC++ at NERSC.
